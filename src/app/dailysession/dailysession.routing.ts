import { Routes } from '@angular/router';

import { DailysessionListComponent } from './list/list.component';
import { DailysessionAddComponent } from './add/add.component';

export const DailysessionRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'list',
            component: DailysessionListComponent
        }, {
            path: 'add',
            component: DailysessionAddComponent
        }]
    }
];
