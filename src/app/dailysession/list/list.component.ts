import { Component } from '@angular/core';
import { DailysessionService } from '../../services/dailysession.service';
import { GeneralService } from '../../provider/general.service';

import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-table-dailysession',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class DailysessionListComponent {

  dailysessions: any;
  dailysession: any;
  

  constructor(private dailysessionService: DailysessionService,
    private generalService: GeneralService
    , private router: Router) {
  }

  ngOnInit() {
    this.dailysessionService.getalldailysession().subscribe(res => {
        this.dailysessions = res;
    },
    err => {
      console.log('Error- ' + err.message);
    });
  }

  edit(id) {

  }

  staff(id) {
  const navigationExtras: NavigationExtras = {
    queryParams: {
        'id': JSON.stringify(id)
    }
  };
    this.router.navigate(['/departments/staff'], navigationExtras);
  }

  delete(id) {

    if (confirm('Are you sure you want to delete this row into the database?')) {
      this.dailysessionService.deletedailysessions(id).subscribe(res => {
          this.generalService.openSnackBar('Deleted');

          this.dailysession = res;
          let index_tmp = -1;
          for (let index = 0; index < this.dailysessions.length; index++) {
            const element = this.dailysessions[index];
            if (element.id === this.dailysession.id) {
              index_tmp = index;
            }
          }

          if (index_tmp !== -1) {
            this.dailysessions.splice(index_tmp, 1);
          }

      },
      err => {
        this.generalService.openSnackBar('Error- ' + err.message);
        console.log('Error- ' + err.message);
      });
    } else {
        // do nothing
    }
  }

}
