import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { DailysessionService } from '../../services/dailysession.service';
import { GeneralService } from '../../provider/general.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';


@Component({
  selector: 'app-add-dailysession',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class DailysessionAddComponent {

  public form: FormGroup;
  medical_concern_id: any;

  constructor(private dailysessionService: DailysessionService
    , private fb: FormBuilder
    , private generalService: GeneralService
    , private route: ActivatedRoute
    , private router: Router) {
    
    this.route.queryParams.subscribe(params => {
      this.medical_concern_id = JSON.parse(params['id']);
    });
    
  }

  ngOnInit() {
    this.form = this.fb.group({
      TargetSkillWorking: [null, Validators.required],
      Note: [null],
      StaffId: [null],
      MedicalConcernId: [null],
      GuidDepartment: [null]
    });

  }

  submit() {

    this.form.value.StaffId = this.generalService.user_info.id;
    this.form.value.GuidDepartment = this.generalService.user_info.department.guid;
    this.form.value.MedicalConcernId = this.medical_concern_id;
   
    this.dailysessionService.adddailysession(this.form.value).subscribe(res => {
      this.generalService.openSnackBar('Saved');
      this.router.navigate(['/apps/calendar']);
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
      console.log('Error- ' + err.message);
    });
  }


}
