import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {
  MatIconModule,
  MatCardModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatTabsModule,
  MatRadioModule,
  MatListModule,
  MatProgressBarModule,
  MatSlideToggleModule,
  MatSelectModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { DailysessionRoutes } from './dailysession.routing';
import { DailysessionListComponent } from './list/list.component';
import { DailysessionAddComponent } from './add/add.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DailysessionRoutes),
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatTabsModule,
    MatListModule,
    MatSlideToggleModule,
    MatSelectModule,
    FlexLayoutModule,
    FormsModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    FileUploadModule,
    MatRadioModule
  ],
  declarations: [
    DailysessionListComponent,
    DailysessionAddComponent,
  ]
})

export class DailysessionModule {}
