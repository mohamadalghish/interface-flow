import { Component } from '@angular/core';
import { MedicalconcernService } from '../../services/medicalconcern.service';
import { GeneralService } from '../../provider/general.service';
import { forEach } from '@angular/router/src/utils/collection';

import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-table-medicalconcern',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class MedicalconcernListNewComponent {

  medicalconcerns: any;
  selected: any[] = [];
  element: any;

  constructor(private medicalconcernService: MedicalconcernService
    , private router: Router
    , private generalService: GeneralService) {
  }

  ngOnInit() {
    this.medicalconcernService.getallmedicalconcernsnew().subscribe(res => {
        this.medicalconcerns = res;
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
      console.log('Error- ' + err.message);
    });
  }

  edit(idx, client_id) {
    // alert(idx);
    const navigationExtras: NavigationExtras = {
      queryParams: {
          'id': JSON.stringify(client_id),
          'medical_concern_id': JSON.stringify(idx)
      }
    };
    this.router.navigate(['/medicalconcerns/add'], navigationExtras);
  }

}
