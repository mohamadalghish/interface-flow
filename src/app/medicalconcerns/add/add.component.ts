import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { MedicalconcernService } from '../../services/medicalconcern.service';
import { ClientService } from '../../services/client.service';
import { DepartmentService } from '../../services/department.service';
import { GeneralService } from '../../provider/general.service';

import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';


@Component({
  selector: 'app-add-medicalconcern',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class MedicalconcernAddComponent implements OnInit{

  list_service = [];
  data_client: any = {
    nameClient : '',
    nameMother : '',
    nameFather : '',
    age : '',
    gender : '',
  };
  list_departmet: any;
  id_service = 1;
  isNonLinear = false;
  data_id: any;
  medical_concern_id: any;
  data_departmet: any;
  staffId: any;
  Theme = 'primary';

  public form: FormGroup;
  constructor(private medicalconcernService: MedicalconcernService,
    private clientService: ClientService,
    private departmentService: DepartmentService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private generalService: GeneralService
    , private router: Router) {

    this.staffId = this.generalService.user_info.id;
    this.route.queryParams.subscribe(params => {
      this.data_id = JSON.parse(params['id']);
      this.medical_concern_id = JSON.parse(params['medical_concern_id']);
    });

    //
    this.clientService.getclient(this.data_id).subscribe(res => {
      this.data_client = res;
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
      console.log('Error- ' + err.message);
    });

    // TODO need replace
    // this.departmentService.getmedicaldepartments().subscribe(res => {
    //   this.list_departmet = res;
    // },
    // err => {
    //   this.generalService.openSnackBar('Error- ' + err.message);
    //   console.log('Error- ' + err.message)
    // });

    //
    this.medicalconcernService.updatemedicalconcernsstatus(this.medical_concern_id, this.staffId, 1).subscribe(res => {
      this.generalService.openSnackBar('Open');
    },
    err => {
      this.generalService.openSnackBar('This process is working on it');
      this.router.navigate(['/medicalconcerns/list']);
      console.log('Error- ' + err.message);
    });

  }

  add_service() {
    this.list_service.push({
      id: this.id_service++,
      Type: '',
      Name : '',
      ContactInfo : '',
      DateCommencement : '',
      DateEnd : '',
      HoursDayWeek : '',
      Report: ''
     });
  }

  remove_service(id) {
    const tmpArray = this.list_service.filter(function( obj ) {
        return obj.id !== id;
    });
    this.list_service = tmpArray;
  }

  /** Returns a FormArray with the name 'formArray'. */
  get formArray(): AbstractControl | null { return this.form.get('formArray'); }

  ngOnInit() {

    this.form = this.fb.group({
      formArray: this.fb.array([
        this.fb.group({
          PregnanyDelivery: [null],
          MedicalCondition: [null],
          CurrMedications: [null],
          Allergies: [null],
          SleepPattern: [null],
          EatingHabits: [null],
          Other: [null],
        }),
        this.fb.group({
          TeacherReportSocialSkill: [null],
          TeacherReportAcademicSkill: [null],
        }),
        this.fb.group({
          ReceptiveLanguageSkill: [null],
          ExpressiveLanguageSkill: [null],
          CurrEchoicSkill: [null],
        }),
        this.fb.group({
          EyeContact: [null],
          PeerInteraction: [null],
        }),
        this.fb.group({
          PhDevelRolling: [null],
          PhDevelCraw: [null],
          PhDevelWalk: [null],
          CommSkillFword: [null],
          AdaptiveSkillDress: [null],
          AdaptiveSkillFeed: [null],
          AdaptiveSkillToilet: [null],
        }),
        this.fb.group({
          EducationInfoSchoolname: [null],
          EducationInfoGrade: [null],
          EducationInfoTeacherReportBehavior: [null],
          EducationInfoAdult: [null],
        }),
        this.fb.group({
          BehaviorConcernAny: [null],
          BehaviorConcernEngageRepetitive: [null],
          BehaviorConcernEngageAggressive: [null],
          BehaviorConcvernSituationCompliant: [null],
          BehaviorConcernEngageHarmful: [null],
          BehaviorConcernEngageUnsafeBehavior: [null],
        }),
        this.fb.group({
          FuncSkillGross: [null],
          FuncSkillFine: [null],
          FuncSkillEatings: [null],
          FuncSkillGroom: [null],
          FuncSkillToilet: [null],
        }),
        this.fb.group({
          PlayPreferredActivities: [null],
          PlayType: [null],
          PlayInteractive: [null],
          PlayFunctional: [null],
          PlayImaginative: [null],
        }),
        this.fb.group({
          PrioritGoals: [null],
          PrioritExpectation: [null],
          PrioritOther: [null],
        }),
        this.fb.group({
          Recommendation: [null],
        }),
        this.fb.group({
          data_departmet: [null],
        })
      ])
    });



  }

  submit() {
    let to_dep_staff_id = '';

    this.data_departmet.forEach(function(element) {
      to_dep_staff_id += element.guid + ',';
    });
    const client = {
      id: this.data_id
    };
    const departmentGuid = this.generalService.user_info.department.guid;
    const staffGuid = this.generalService.user_info.guid;

    const medicalConcern = {
      id: this.medical_concern_id,
      ClientId: this.data_id,
      StaffId: parseInt(this.generalService.user_info.id),
      PregnanyDelivery: this.form.value.formArray[0].PregnanyDelivery,
      MedicalCondition: this.form.value.formArray[0].MedicalCondition,
      CurrMedications: this.form.value.formArray[0].CurrMedications,
      Allergies: this.form.value.formArray[0].Allergies,
      SleepPattern: this.form.value.formArray[0].SleepPattern,
      EatingHabits: this.form.value.formArray[0].EatingHabits,
      Other: this.form.value.formArray[0].Other,

      TeacherReportSocialSkill: this.form.value.formArray[1].TeacherReportSocialSkill,
      TeacherReportAcademicSkill: this.form.value.formArray[1].TeacherReportAcademicSkill,

      ReceptiveLanguageSkill: this.form.value.formArray[2].ReceptiveLanguageSkill,
      ExpressiveLanguageSkill: this.form.value.formArray[2].ExpressiveLanguageSkill,
      CurrEchoicSkill: this.form.value.formArray[2].CurrEchoicSkill,

      EyeContact: this.form.value.formArray[3].EyeContact,
      PeerInteraction: this.form.value.formArray[3].PeerInteraction,

      PhDevelRolling: this.form.value.formArray[4].PhDevelRolling,
      PhDevelCraw: this.form.value.formArray[4].PhDevelCraw,
      PhDevelWalk: this.form.value.formArray[4].PhDevelWalk,
      CommSkillFword: this.form.value.formArray[4].CommSkillFword,
      AdaptiveSkillDress: this.form.value.formArray[4].AdaptiveSkillDress,
      AdaptiveSkillFeed: this.form.value.formArray[4].AdaptiveSkillFeed,
      AdaptiveSkillToilet: this.form.value.formArray[4].AdaptiveSkillToilet,

      EducationInfoSchoolname: this.form.value.formArray[5].EducationInfoSchoolname,
      EducationInfoGrade: this.form.value.formArray[5].EducationInfoGrade,
      EducationInfoTeacherReportBehavior: this.form.value.formArray[5].EducationInfoTeacherReportBehavior,
      EducationInfoAdult: this.form.value.formArray[5].EducationInfoAdult,

      BehaviorConcernAny: this.form.value.formArray[6].BehaviorConcernAny,
      BehaviorConcernEngageRepetitive: this.form.value.formArray[6].BehaviorConcernEngageRepetitive,
      BehaviorConcernEngageAggressive: this.form.value.formArray[6].BehaviorConcernEngageAggressive,
      BehaviorConcvernSituationCompliant: this.form.value.formArray[6].BehaviorConcvernSituationCompliant,
      BehaviorConcernEngageHarmful: this.form.value.formArray[6].BehaviorConcernEngageHarmful,
      BehaviorConcernEngageUnsafeBehavior: this.form.value.formArray[6].BehaviorConcernEngageUnsafeBehavior,

      FuncSkillGross: this.form.value.formArray[7].FuncSkillGross,
      FuncSkillFine: this.form.value.formArray[7].FuncSkillFine,
      FuncSkillEatings: this.form.value.formArray[7].FuncSkillEatings,
      FuncSkillGroom: this.form.value.formArray[7].FuncSkillGroom,
      FuncSkillToilet: this.form.value.formArray[7].FuncSkillToilet,

      PlayPreferredActivities: this.form.value.formArray[8].PlayPreferredActivities,
      PlayType: this.form.value.formArray[8].PlayType,
      PlayInteractive: this.form.value.formArray[8].PlayInteractive,
      PlayFunctional: this.form.value.formArray[8].PlayFunctional,
      PlayImaginative: this.form.value.formArray[8].PlayImaginative,

      PrioritGoals: this.form.value.formArray[9].PrioritGoals,
      PrioritExpectation: this.form.value.formArray[9].PrioritExpectation,
      PrioritOther: this.form.value.formArray[9].PrioritOther,

      Recommendation: this.form.value.formArray[10].Recommendation,

      ToDepStaffId: to_dep_staff_id
    };

    const client_MedicalConcern = {
      Client: client,
      MedicalConcern: medicalConcern,
      DepartmentGuid: departmentGuid,
      StaffGuid: staffGuid
    };
    console.log(medicalConcern);
    this.medicalconcernService.modifyMedicalconcern(client_MedicalConcern, this.medical_concern_id).subscribe(res => {
      this.router.navigate(['/medicalconcerns/list']);
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
      console.log('Error- ' + err.message);
    });
  }


}
