import { Routes } from '@angular/router';

import { MedicalconcernListComponent } from './list/list.component';
import { MedicalconcernAddComponent } from './add/add.component';
import { MedicalconcernEditComponent } from './edit/edit.component';
import { MedicalconcernListNewComponent } from './new/new.component';

export const MedicalconcernsRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'list',
            component: MedicalconcernListComponent
        }, {
            path: 'add',
            component: MedicalconcernAddComponent
        }, {
            path: 'new',
            component: MedicalconcernListNewComponent
        }, {
            path: 'edit',
            component: MedicalconcernEditComponent
        }]
    }
];
