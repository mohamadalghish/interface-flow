import { Component, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MedicalconcernService } from '../../services/medicalconcern.service';
import { DepartmentService } from '../../services/department.service';
import { GeneralService } from '../../provider/general.service';

import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

// Dialgo import
import { MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-calendar-dialog',
  template: `
  <h5 class='mt-0'></h5>
  <div>
  <table width='100%'>
    <thead>
      <td>Department</td>
      <td>Status</td>
    </thead>
    <tr *ngFor='let item of data.list; let i = index'>
      <td>
        {{getNameByGuid(item.guid)}}
      </td>
      <td>
        {{item.done}}
      </td>
    </tr>
  </table>
  </div>
  <button md-button type='button' (click)='dialogRef.close()'>Close dialog</button>`
})
export class MedicalconcernDialogComponentTransform {
  constructor(
    public dialogRef: MatDialogRef<MedicalconcernDialogComponentTransform>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

    getNameByGuid(guid) {
      for (let i = 0; i < this.data.department.length; i++) {
        if (this.data.department[i].guid === guid) {
            return this.data.department[i].name;
        }
      }
    }
}
//

export interface DemoColor {
  name: string;
  color: string;
}

@Component({
  selector: 'app-table-medicalconcern',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class MedicalconcernListComponent {

  dialogRef: MatDialogRef<MedicalconcernDialogComponentTransform>;
  lastCloseResult: string;
  actionsAlignment: string;
  config: MatDialogConfig = {
    disableClose: false,
    width: '',
    height: '',
    position: {
      top: '',
      bottom: '',
      left: '',
      right: ''
    },
    data: {
      action: '',
      event: []
    }
  };

  medicalconcerns: any;
  departments: any;


  constructor(private medicalconcernService: MedicalconcernService,
    private departmentService: DepartmentService
    , public dialog: MatDialog
    , private router: Router
    , private generalService: GeneralService) {
  }

  ngOnInit() {

    this.medicalconcernService.getallmedicalconcerns().subscribe(res => {
      this.medicalconcerns = res;
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
      console.log('Error- ' + err.message);
    });

    // TODO need replace
    // this.departmentService.getmedicaldepartments().subscribe(res => {
    //     this.departments = res;
    // },
    // err => {
    //   this.generalService.openSnackBar('Error- ' + err.message);
    //   console.log('Error- ' + err.message);
    // });


  }

  show_transaction(idx) {

    this.medicalconcernService.getmedicalconcernsforwadlist(idx).subscribe(res => {
      this.config.data = {list: res, department: this.departments};
      this.dialogRef = this.dialog.open(MedicalconcernDialogComponentTransform, this.config);

      this.dialogRef.afterClosed().subscribe((result: string) => {
        this.lastCloseResult = result;
        this.dialogRef = null;
      });
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
      console.log('Error- ' + err.message);
    });


  }

  edit(idx, client_id) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
          'id': JSON.stringify(client_id),
          'medical_concern_id': JSON.stringify(idx)
      }
    };

    this.router.navigate(['/medicalconcerns/edit'], navigationExtras);
  }

}
