import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { MedicalconcernService } from '../../services/medicalconcern.service';
import { ClientService } from '../../services/client.service';
import { DepartmentService } from '../../services/department.service';
import { GeneralService } from '../../provider/general.service';
import { ScheduleService } from '../../services/schedule.service';

import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

// Calendar import
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';

import { Subject } from 'rxjs/Subject';

import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};
//

@Component({
  selector: 'app-edit-medicalconcern',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class MedicalconcernEditComponent implements OnInit{


  public form: FormGroup;
  data_id: any;
  medical_concern_id: any;
  data_medical_concern : any = {
      id: '',
      clientId: '',
      staffId: '',
      pregnanyDelivery: '',
      medicalCondition: '',
      currMedications: '',
      allergies: '',
      sleepPattern: '',
      eatingHabits: '',
      other: '',
      teacherReportSocialSkill: '',
      teacherReportAcademicSkill: '',

      receptiveLanguageSkill: '',
      expressiveLanguageSkill: '',
      currEchoicSkill: '',
      eyeContact: '',
      peerInteraction: '',

      phDevelRolling: '',
      phDevelCraw: '',
      phDevelWalk: '',
      commSkillFword: '',
      adaptiveSkillDress: '',
      adaptiveSkillFeed: '',
      adaptiveSkillToilet: '',

      educationInfoSchoolname: '',
      educationInfoGrade: '',
      educationInfoTeacherReportBehavior: '',
      educationInfoAdult: '',

      behaviorConcernAny: '',
      bBehaviorConcernEngageRepetitive: '',
      behaviorConcernEngageAggressive: '',
      behaviorConcvernSituationCompliant: '',
      behaviorConcernEngageHarmful: '',
      behaviorConcernEngageUnsafeBehavior: '',

      funcSkillGross: '',
      funcSkillFine: '',
      funcSkillEatings: '',
      funcSkillGroom: '',
      funcSkillToilet: '',

      playPreferredActivities: '',
      playType: '',
      playInteractive: '',
      playFunctional: '',
      playImaginative: '',

      prioritGoals: '',
      prioritExpectation: '',
      prioritOther: '',
      recommendation: '',
  };

  data_client : any = {
    nameClient : '',
    nameMother : '',
    nameFather : '',
    age : '',
    gender : '',
  };

  departments: any;
  items: any[] = [];
  calendar_data: any;
  items_hours_dept: any[] = [];
  items_schedule: any[] = [];

  public dic_departments = {};
  public dictionary = {};

  // Calendar variable
  view = 'month';
  viewDate: Date = new Date();
  activeDayIsOpen = true;
  events: CalendarEvent[] = [];
  refresh: Subject<any> = new Subject();

  // Calendar functions
  show_calendar = false
  dayClicked({date, events}: {date: Date, events: CalendarEvent[]}): void {

    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  addEvent(obj): void {

    let day = new Date(obj.date);
    let start_time_date = new Date(obj.timeFrom);
    let end_time_date = new Date(obj.timeTo);

    this.events.push({
      title: 'Client ' + obj.client + ', Staff ' + obj.staf_name + ', Department ' + obj.department_name + ', Duration ' + obj.duration,
      start: new Date(day.getFullYear(), day.getMonth(), day.getDate(), start_time_date.getHours(), start_time_date.getMinutes(), start_time_date.getSeconds(), start_time_date.getMilliseconds()),
      end: new Date(day.getFullYear(), day.getMonth(), day.getDate(), end_time_date.getHours(), end_time_date.getMinutes(), end_time_date.getSeconds(), end_time_date.getMilliseconds()),
      color: this.generalService.colors[obj.color],
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      }
    });

  }

  constructor(private clientService: ClientService,
    private medicalconcernService: MedicalconcernService,
    private departmentService: DepartmentService,
    private scheduleService: ScheduleService,
    private fb: FormBuilder,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router) {


    this.route.queryParams.subscribe(params => {
      this.data_id = JSON.parse(params['id']);
      this.medical_concern_id = JSON.parse(params['medical_concern_id']);
    });


    // TODO need replace
    // this.departmentService.getmedicaldepartments().subscribe(res => {
    //   this.departments = res;

    //   this.departments.forEach(element => {
    //     this.dic_departments[element.id] = element.name;
    //   });

    //   this.getmedicalconcernstreatment();
      
    // },
    // err => {
    //   this.generalService.openSnackBar(err.message);
    //   console.log('Error- ' + err.message);
    // });


    

    this.clientService.getclient(this.data_id).subscribe(res => {
      this.data_client = res;
    },
    err => {
      this.generalService.openSnackBar(err.message);
      this.router.navigate(['/medicalconcerns/list']);
      console.log('Error- ' + err.message);
    });

  }

  getmedicalconcernstreatment() {
    this.medicalconcernService.getmedicalconcernstreatment(this.medical_concern_id).subscribe(res => {
      this.data_medical_concern = res;

      this.data_medical_concern.treatment.forEach(element => {

        const department_id = element.departmentId;
        let sum_hours = 0;

        element.hoursTreatment.forEach(element => {
          sum_hours += element.hours;
          this.items.push(
              {
                Text: element.text,
                Hours: element.hours,
                Note: element.note
              }
            );
        });

        if (this.dictionary[department_id] !== undefined) {
          this.dictionary[department_id] += sum_hours;
        } else {
          this.dictionary[department_id] = sum_hours;
        }

      });

      for (var key in this.dictionary) {
        if (this.dictionary.hasOwnProperty(key)) {
          this.items_hours_dept.push(
            {
              Id: key,
              Key: this.dic_departments[key],
              Value: this.dictionary[key]
            }
          );
        }
      }

    },
    err => {
      let erroMessage = err.message;
      if (err.error !== undefined) {
        erroMessage = err.error.message;
      }
      this.generalService.openSnackBar(erroMessage);
      this.router.navigate(['/medicalconcerns/list']);
    });
  }

  ngOnInit() {

  }

  submit() {
    const scheduleList = {
      listSchedule: this.items_schedule,
    };

    this.scheduleService.approvalCalendar(this.medical_concern_id, scheduleList).subscribe(res => {
      this.generalService.openSnackBar('Saved');
      this.router.navigate(['/medicalconcerns/list']);
    },
    err => {
      this.generalService.openSnackBar(err.message);
      console.log('Error- ' + err.message);
    });
  }

  generateCalendar(index) {

    const element = this.items_hours_dept[index];
    let HoursWeek = {
      ClientId: this.data_id,
      DepartmentId: parseInt(element.Id),
      Hours: element.Value
    }

    this.scheduleService.generateCalendar(HoursWeek).subscribe(res => {
      this.calendar_data = res;
      this.calendar_data.forEach(element => {
        const obj = {
          client : element.clientId,
          date : element.date,
          timeFrom : element.timeFrom,
          timeTo : element.timeTo,
          duration : element.hour,
          staf_name : element.scheduleStuff[0].staff.name,
          department_name : element.scheduleStuff[0].staff.department.name,
          department_id : element.scheduleStuff[0].staff.departmentId,
          color: element.scheduleStuff[0].staff.department.color
        }

        this.addEvent(obj);
        this.items_schedule.push( element.id);
      });

      this.show_calendar = true;
      this.refresh.next();
    },
    err => {
      this.generalService.openSnackBar(err.message);
      console.log('Error- ' + err.message);
    });

  }

  addABunch() {

    this.items.push(
      {
        Text: '',
        Hours: 0,
        Note: ''
      }
    );

  }

  removeABunch(idx) {

    if (idx > -1) {
      this.items.splice(idx, 1);
    }

  }


}
