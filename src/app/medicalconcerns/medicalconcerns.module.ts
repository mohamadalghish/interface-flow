import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {
  MatIconModule,
  MatCardModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatTabsModule,
  MatRadioModule,
  MatListModule,
  MatProgressBarModule,
  MatSlideToggleModule,
  MatSelectModule,
  MatDialogModule,
  MatChipsModule,
  MatStepperModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { CalendarModule, CalendarDateFormatter } from 'angular-calendar';

import { MedicalconcernsRoutes } from './medicalconcerns.routing';
import { MedicalconcernListComponent } from './list/list.component';
import { MedicalconcernListNewComponent } from './new/new.component';
import { MedicalconcernAddComponent } from './add/add.component';
import { MedicalconcernEditComponent } from './edit/edit.component';
import { MedicalconcernDialogComponentTransform } from './list/list.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MedicalconcernsRoutes),
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatTabsModule,
    MatListModule,
    MatSlideToggleModule,
    MatSelectModule,
    FlexLayoutModule,
    FormsModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    FileUploadModule,
    CalendarModule.forRoot(),
    MatRadioModule,
    MatStepperModule,
    MatChipsModule,
    MatDialogModule
  ],
  declarations: [
    MedicalconcernListComponent,
    MedicalconcernAddComponent,
    MedicalconcernListNewComponent,
    MedicalconcernEditComponent,
    MedicalconcernDialogComponentTransform,
  ],
  entryComponents: [ MedicalconcernDialogComponentTransform ],
})

export class MedicalconcernsModule {}
