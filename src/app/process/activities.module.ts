import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {
  MatIconModule,
  MatCardModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatTabsModule,
  MatRadioModule,
  MatListModule,
  MatProgressBarModule,
  MatSlideToggleModule,
  MatSelectModule,
  MatSnackBarModule,
  MatDatepickerModule,
  MatCheckboxModule,
  MatNativeDateModule,
  MatAutocompleteModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { ActivitiesRoutes } from './activities.routing';

import { DynamicFormBuilderModule } from './dynamic-form-builder/dynamic-form-builder.module';
import { AppComponent } from './app.component';
import { ProcessListComponent } from './list/list.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ActivitiesRoutes),
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatTabsModule,
    MatListModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatSnackBarModule,
    FlexLayoutModule,
    FormsModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    FileUploadModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    DynamicFormBuilderModule
  ],
  declarations: [
    AppComponent,
    ProcessListComponent
  ]
})

export class ActivitiesModule {}
