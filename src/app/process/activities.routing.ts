import { Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { ProcessListComponent } from './list/list.component';

export const ActivitiesRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'my_activity',
            component: AppComponent
        }, {
            path: 'list',
            component: ProcessListComponent
        }]
    }
];
