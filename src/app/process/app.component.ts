import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { ProcessService } from '../services/process.service';
import { GeneralService } from '../provider/general.service';
import { map } from 'rxjs/operators';

import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http'

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public form: FormGroup;
  unsubcribe: any
  public objActivity: any;
  public objActivities:  any[] = [];
  objArrows: any;
  public 

  public arrowNext: any;
  public arrowPrev: any;

  private _processId : any;

  public fields: any[] = [    
  ]
  public files: any[] = [
  ];
  public formDatas: any[] = [
  ];
  private _result: any;
  color: "primary";
  alignment: "start";


  constructor(private processService: ProcessService,
    private fb: FormBuilder,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
    ,private http: HttpClient) {

      if (this.generalService.user_info == undefined || this.generalService.user_info == null) {
        this.generalService.get_user_info_localy();
      }    
  }

  ngOnInit() {
    
    this.route.queryParams.subscribe(params => {
      this._processId = JSON.parse(params['processId']);
    });

    this.processService.get_current_activity(this._processId, 
      this.generalService.user_info.groupUsers[0].id).subscribe(res => {
      
      this._result = res;
      this._result.data.forEach( (element, index) => {
        //
        let obj = {};

        obj['Activity'] = element;
        if (this._result.multiObject) {
          
          if (this._result.map['Arrows_' + element.id].length > 0) {
            obj['Arrows'] = this._result.map['Arrows_' + element.id][0];
            obj['arrowNext'] = obj['Arrows'].activitiesNext;
            obj['arrowPrev'] = obj['Arrows'].activitiesPrev;
            obj['condition'] = obj['Arrows'].condition;
          }
        }
        
        obj['fields'] = [];
        obj['form'] = this.fb.group({
        })

        obj['Activity'].variables.forEach( (item, index) => {
          
          let itemObject = {
            uuid: item.id,
            type: item.type,
            name: item.name,
            placeholder: item.name,
            label: item.name,
            value: item.value,
            required: item.required,
          }
          
          
          if ( item.type === 'radio' ) {
            
            itemObject['options'] = item.variableOptionValues;
          } else if (item.type === 'file') {

          } else {
            // do something 
          }
          
          obj['fields'].push(
            itemObject
          )
          obj['form'].addControl(item.name, this.fb.control(item.value, (item.required? Validators.required : null) ));
          
        });

        this.objActivities.push(obj);
      });
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
      console.log('Error- ' + err.message);
      return;
    });


  }

  submit(obj) {

    for (const formData of this.formDatas) {
      
      this.processService.upload_files(formData).subscribe(res => {

      },
      err => {
        this.generalService.openSnackBar('Error- ' + err.message);
        console.log('Error- ' + err.message);
        return;
      });
    }

    let multiArrow = false;
    console.log(obj['form'].value);

    obj['Activity'].variables.forEach( (element, index) => {

      obj['Activity'].variables[index].value = obj['form'].value[element.name];
    })

    if (obj['arrowNext'].length > 1) {
      multiArrow = true
    }

    for (let j = 0; j < obj['arrowNext'].length; j++) {
      
      const iterator = obj['arrowNext'][j];
      let conditionItem = 
      {
        value: ''
      };

      if ( obj['condition'].length > 0 ) {
        conditionItem = obj['condition'][j];
      }
      
      this.processService.excute_activity(obj['Activity'], iterator.id, multiArrow, conditionItem.value).subscribe(res => {
        
        // success
        for (const formData of this.formDatas) {
          
          this.processService.upload_files(formData).subscribe(res => {

          },
          err => {
            this.generalService.openSnackBar('Error- ' + err.message);
            console.log('Error- ' + err.message);
            return;
          });
        }
      },
      err => {
        this.generalService.openSnackBar('Error- ' + err.message);
        console.log('Error- ' + err.message);
        return;
      });
    }
    
    this.generalService.openSnackBar('The operation has been sent');
    this.router.navigate(['/process/list'])
  }

  getFields() {
    return this.fields;
  }

  // file functions
  delete_file(id) {
    
    if (id != null) {

      for(var i = this.files.length - 1; i >= 0; i--) {

        if(this.files[i].id === id) {
          
          this.formDatas.splice(i, 1);
          this.files.splice(i, 1);
        }
      }
    }
  }

  upload(files, activityId, varaiableId) {
    
    let UUID = this.generalService.uuidv4();
    if (files.length === 0)
      return;

   

    for (let file of files)
    {
      const variableFile = {
        guidVariable: varaiableId,
        guidActivitty: activityId
      }
      
      const formData = new FormData();
      formData.append('file', file);
      formData.append('helperVariableFile', JSON.stringify(variableFile));
      this.formDatas.push(formData);
      this.files.push({
        id: UUID,
        name: file.name,
        length: file.size,
        contentType: file.type,
        date: new Date()
      })
    }
  }

  ngDistroy() {
    this.unsubcribe();
  }
}
