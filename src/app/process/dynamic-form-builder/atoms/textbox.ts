import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

// text,email,tel,textarea,password, 
@Component({
    selector: 'textbox',
    template: `
    <div class="mb-1">
          <mat-form-field style="width: 100%">
              <input matInput 
               [attr.type]="field.type"
               [id]="field.name" 
               [name]="field.name"
               [placeholder]="field.placeholder" [formControl]="form.controls[field.name]">
          </mat-form-field>
          <small *ngIf="form.controls[field.name].hasError('required') && form.controls[field.name].touched" class="mat-text-warn">Field is required</small>
    </div>
    `
})
export class TextBoxComponent {
    @Input() field:any = {};
    @Input() form:FormGroup;
    get isValid() { return this.form.controls[this.field.name].valid; }
    get isDirty() { return this.form.controls[this.field.name].dirty; }
  
    constructor() {
        
    }
}