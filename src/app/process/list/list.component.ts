import { Component } from '@angular/core';
import { ProcessService } from '../../services/process.service';
import { GeneralService } from '../../provider/general.service';

import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-table-department',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ProcessListComponent {

  result: any;
  process: any;

  listStatusProcess: any[] = 
  [{
    id: 0,
    name : 'inactive'
  }, {
    id: 1,
    name : 'active'
  }, {
    id: 2,
    name : 'pending'
  }];

  constructor(private processService: ProcessService,
    private generalService: GeneralService
    , private router: Router) {

      if (this.generalService.user_info == undefined || this.generalService.user_info == null) {
        this.generalService.get_user_info_localy();
      }

      // const str = '{"id": "5b4227c1-f746-466b-b675-5cda29cb3894", "name": "Patient Flow", "status": 0, "activities": [{"id": "68460b84-8639-4912-8031-0fd0e31385fc", "name": "RCCP", "status": 1, "tag": 0, "groupUsers": [{"id": "bb64815a-a7d0-4cd6-9a8b-5d48326de612"} ], "variables": [{"id": "f34073c8-7999-4d4c-927e-f76b5751d325", "name": "var1", "type": "text", "value": ""} ] }, {"id": "b4a84c03-46e1-4948-8c3a-b4b1116d6146", "name": "A.B.A Interview with Parent", "status": 0, "tag": 1, "groupUsers": [{"id": "e07f3c06-9baf-458e-a8fa-21a2e8772608"} ], "variables": [{"id": "f34073c8-7999-4d4c-927e-f76b5751d325", "name": "var1", "type": "text", "value": ""} ] }, {"id": "899a94cf-ab3b-4aae-9271-868b1676a8ea", "name": "A.B.A Interview with Child", "status": 0, "tag": 1, "groupUsers": [{"id": "e07f3c06-9baf-458e-a8fa-21a2e8772608"} ], "variables": [{"id": "f34073c8-7999-4d4c-927e-f76b5751d325", "name": "var1", "type": "text", "value": ""} ] }, {"id": "ead0b784-3103-489d-b429-0117c078166b", "name": "OCC", "status": 0, "tag": 1, "groupUsers": [{"id": "c20d8d30-af7a-4e47-99cd-a058dba5e392"} ], "variables": [{"id": "f34073c8-7999-4d4c-927e-f76b5751d325", "name": "var1", "type": "text", "value": ""} ] }, {"id": "288b1d07-2203-4390-ab04-73d4adb664f8", "name": "S.E", "status": 0, "tag": 1, "groupUsers": [{"id": "a995bbb8-f0ff-4c02-8a4a-bb3c5b2cbf5a"} ], "variables": [{"id": "f34073c8-7999-4d4c-927e-f76b5751d325", "name": "var1", "type": "text", "value": ""} ] }, {"id": "8b719e9f-6fc9-4c59-b13c-ede2ccc25e18", "name": "L.S", "status": 0, "tag": 1, "groupUsers": [{"id": "85b55443-650e-4a3c-bcfa-0f897cb5924c"} ], "variables": [{"id": "f34073c8-7999-4d4c-927e-f76b5751d325", "name": "var1", "type": "text", "value": ""} ] }, {"id": "77c5db28-8b2e-4bc8-ab2d-9e109d0d24b1", "name": "Psychotherapy", "status": 0, "tag": 1, "groupUsers": [{"id": "14aa3294-748f-4a2e-b5dd-5debee2fc464"} ], "variables": [{"id": "f34073c8-7999-4d4c-927e-f76b5751d325", "name": "var1", "type": "text", "value": ""} ] }, {"id": "63972133-fc94-47d2-8727-9a8d50cefe28", "name": "A.B.A", "status": 0, "tag": 2, "groupUsers": [{"id": "e07f3c06-9baf-458e-a8fa-21a2e8772608"} ], "variables": [{"id": "f34073c8-7999-4d4c-927e-f76b5751d325", "name": "var1", "type": "text", "value": ""} ] } ], "arrows": [{"id": "41291a41-b9bc-447a-9662-6a8f7cb7ffe7", "name": "next", "activityCurrent": [{"id": "68460b84-8639-4912-8031-0fd0e31385fc"} ], "activitiesNext": [{"id": "b4a84c03-46e1-4948-8c3a-b4b1116d6146"} ], "activitiesPrev": [], "condition": "true"}, {"id": "dd0626bf-bb2a-4cad-a048-30679d29ec51", "name": "next", "activityCurrent": [{"id": "b4a84c03-46e1-4948-8c3a-b4b1116d6146"} ], "activitiesNext": [{"id": "899a94cf-ab3b-4aae-9271-868b1676a8ea"} ], "activitiesPrev": [{"id": "68460b84-8639-4912-8031-0fd0e31385fc"} ], "condition": "true"}, {"id": "c0e5b477-d887-4979-ac4b-e92106d27e80", "name": "next", "activityCurrent": [{"id": "899a94cf-ab3b-4aae-9271-868b1676a8ea"} ], "activitiesNext": [{"id": "ead0b784-3103-489d-b429-0117c078166b"}, {"id": "288b1d07-2203-4390-ab04-73d4adb664f8"}, {"id": "8b719e9f-6fc9-4c59-b13c-ede2ccc25e18"}, {"id": "77c5db28-8b2e-4bc8-ab2d-9e109d0d24b1"} ], "activitiesPrev": [{"id": "b4a84c03-46e1-4948-8c3a-b4b1116d6146"} ], "condition": "true"}, {"id": "5467960f-8f0f-406c-b7e6-bb79ff5c271c", "name": "done", "activityCurrent": [{"id": "ead0b784-3103-489d-b429-0117c078166b"} ], "activitiesNext": [{"id": "63972133-fc94-47d2-8727-9a8d50cefe28"} ], "activitiesPrev": [{"id": "899a94cf-ab3b-4aae-9271-868b1676a8ea"} ], "condition": "true"}, {"id": "5fdec48b-9b7c-4cad-b1d3-7b837fb25e7b", "name": "done", "activityCurrent": [{"id": "288b1d07-2203-4390-ab04-73d4adb664f8"} ], "activitiesNext": [{"id": "63972133-fc94-47d2-8727-9a8d50cefe28"} ], "activitiesPrev": [{"id": "899a94cf-ab3b-4aae-9271-868b1676a8ea"} ], "condition": "true"}, {"id": "0c220e4c-21b7-4488-bfdb-f0cb61ac1927", "name": "done", "activityCurrent": [{"id": "77c5db28-8b2e-4bc8-ab2d-9e109d0d24b1"} ], "activitiesNext": [{"id": "63972133-fc94-47d2-8727-9a8d50cefe28"} ], "activitiesPrev": [{"id": "899a94cf-ab3b-4aae-9271-868b1676a8ea"} ], "condition": "true"}, {"id": "8c9fd0f6-d5ab-418e-b8fe-4d0cb11f370c", "name": "done", "activityCurrent": [{"id": "8b719e9f-6fc9-4c59-b13c-ede2ccc25e18"} ], "activitiesNext": [{"id": "63972133-fc94-47d2-8727-9a8d50cefe28"} ], "activitiesPrev": [{"id": "899a94cf-ab3b-4aae-9271-868b1676a8ea"} ], "condition": "true"} ] }'
      
      // this.processService.save_process(str).subscribe(res => {
      //   debugger
      //  },
      //  err => {
      //    this.generalService.openSnackBar('Error- ' + err.message);
      //    console.log('Error- ' + err.message);
      //    return;
      //  });

  }

  ngOnInit() {
   
    this.processService.get_all_process().subscribe(res => {
      this.result = res;
      this.process = this.result.data;
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
      console.log('Error- ' + err.message);
      return;
    });
  }

  get_name_status(id) {
    this.listStatusProcess.forEach(element => {
      if (element.id === id) {
        return element.name;
      }
    });
  }

  edit(id) {

    const navigationExtras: NavigationExtras = {

      queryParams: {
          'processId': JSON.stringify(id)
      }
    };

    this.router.navigate(['/process/my_activity'], navigationExtras);
  }

  delete(id) {
    alert(id)
  }
  // delete(id) {

  //   if (confirm('Are you sure you want to delete this row into the database?')) {
  //     this.departmentService.deletepartments(id).subscribe(res => {
  //         this.generalService.openSnackBar('Deleted');

  //         this.department = res;
  //         let index_tmp = -1;
  //         for (let index = 0; index < this.departments.length; index++) {
  //           const element = this.departments[index];
  //           if (element.id === this.department.id) {
  //             index_tmp = index;
  //           }
  //         }

  //         if (index_tmp !== -1) {
  //           this.departments.splice(index_tmp, 1);
  //         }

  //     },
  //     err => {
  //       this.generalService.openSnackBar('Error- ' + err.message);
  //       console.log('Error- ' + err.message);
  //     });
  //   } else {
  //       // do nothing
  //   }
  // }

}
