import { Injectable } from '@angular/core';
import { GeneralService } from "../../provider/general.service";

import { CookieService } from 'ngx-cookie-service';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  permission_number: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: '/',
    name: 'HOME',
    type: 'link',
    icon: 'explore',
    permission_number: '0',
  }, {
    state: 'apps',
    name: 'Schedule',
    type: 'sub',
    icon: 'calendar_today',
    permission_number: '0',
    children: [
      {state: 'calendar', name: 'CALENDAR'},
    ]
  }, {
    state: 'users',
    name: 'Staff',
    type: 'sub',
    icon: 'supervised_user_circle',
    permission_number: '2',
    children: [
      {state: 'add', name: 'Add Staff'},
      {state: 'list', name: 'Staff List'},
    ],
  }, {
    state: 'departments',
    name: 'Departments',
    type: 'sub',
    icon: 'account_box',
    permission_number: '8',
    children: [
      {state: 'add', name: 'Add Department'},
      {state: 'list', name: 'Department List'},
    ],
  }, {
    state: 'process',
    name: 'Process',
    type: 'sub',
    icon: 'library_add',
    permission_number: '1022',
    children: [
      {state: 'list', name: 'List'},
      // {state: 'my_activity', name: 'Activity'},
    ],
  }
  ];

@Injectable()
export class MenuService {

  constructor(
    public generalService: GeneralService
    , private cookieService: CookieService) {
  }

  getAll(): Menu[] {

    return MENUITEMS;
    
    // if (this.generalService.user_info == undefined) {
     
    //   this.generalService.user_info = {department : {}};
    //   this.generalService.user_info.guid = this.cookieService.get('CUInofGuid');
    //   this.generalService.user_info.id = this.cookieService.get('CUInofId');
    //   this.generalService.user_info.department.permission = this.cookieService.get('CUInofDeptPermission');
    //   this.generalService.user_info.department.guid = this.cookieService.get('CUInofDeptGuid');
    // }

    // if (this.generalService.user_info != undefined) {
    //   var permission = this.generalService.user_info.department.permission;
    //   var return_menu = [];

    //   MENUITEMS.forEach(function(element) {

    //     let n = Number(element.permission_number);
    //     if (n == undefined) {
    //       n = permission;
    //     }
    //     const permission_number = n & permission;

    //     if (permission_number === n) {
    //       return_menu.push(element);
    //     }
    //   });

    //   return return_menu;
    // } else {
    //   return [];
    // }
    
  }

  add(menu: Menu) {
    MENUITEMS.push(menu);
  }
}
