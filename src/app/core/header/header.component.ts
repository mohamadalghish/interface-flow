import { Component, EventEmitter, Output } from '@angular/core';

import * as screenfull from 'screenfull';
import {TranslateService} from '@ngx-translate/core';

import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {

    currentLang = 'en';
    showSettings = false;
    options = {
        collapsed: false,
        compact: false,
        boxed: false,
        dark: false,
        dir: 'ltr'
    };


  @Output() toggleSidenav = new EventEmitter<void>();
  @Output() toggleNotificationSidenav = new EventEmitter<void>();
  @Output() messageEvent = new EventEmitter<Object>();

    constructor(
        public translate: TranslateService
        , private router: Router) {
        const browserLang: string = translate.getBrowserLang();
        translate.use(browserLang.match(/ar|en|fr/) ? browserLang : 'en');
    }
    sendOptions() {
        if (this.options.collapsed === true ) {
            this.options.compact = false;
        }
        if (this.options.compact === true ) {
            this.options.collapsed = false;
        }
        this.messageEvent.emit(this.options);
    }

  fullScreenToggle(): void {
    if (screenfull.enabled) {
      screenfull.toggle();
    }
  }

  logout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/session/signin']);
  }

  profile(): void {
    this.router.navigate(['/apps/social'])
  }
}
