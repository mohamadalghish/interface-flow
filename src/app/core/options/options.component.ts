import { Component, Output, EventEmitter } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { ProcessService } from "../../services/process.service";
import { GeneralService } from "../../provider/general.service";

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html'
})
export class OptionsComponent {

  currentLang = 'en';
  showSettings = false;
  options = {
    collapsed: false,
    compact: false,
    boxed: false,
    dark: false,
    dir: 'ltr'
  };

  result: any;


  @Output() messageEvent = new EventEmitter<Object>();

  constructor(
    private processService: ProcessService,
    private generalService: GeneralService,
    public translate: TranslateService) {
    const browserLang: string = translate.getBrowserLang();
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
  }

  sendOptions() {
    if (this.options.collapsed === true ) {
      this.options.compact = false;
    }
    if (this.options.compact === true ) {
      this.options.collapsed = false;
    }
    this.messageEvent.emit(this.options);
  }

  add_process() {
    this.processService.add_process().subscribe(res => {
      
      this.result = res;
      if (this.result.responseCode == 200) {

      	

      } else {
        this.generalService.openSnackBar('Error- ' + this.result.message);
        console.log('Error- ' + this.result.message);
      }
      
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
      console.log('Error- ' + err.message);
    });
  }
}
