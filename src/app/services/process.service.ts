import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders , HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';

import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class ProcessService {
    private baseapi = environment.apiUrl;
    private baseurl = environment.baseUrl;
    private _controllerProcess = "process/";
    private _controllerActivity = "activity/";

    constructor(private http: HttpClient,
                public auth: AuthService) { }

    get_all_process() {
        return this.http.get(this.baseapi + this._controllerProcess + 'list');
    }

    save_process(obj) {   
        return this.http.post(this.baseapi + this._controllerProcess + "addProcess", obj);
    }

    add_process() {   
        return this.http.get(this.baseapi + this._controllerProcess + "addProcessTemplate");
    }

    get_current_activity(process_id, group_id) {
        return this.http.get(this.baseapi + this._controllerActivity + 'getMyActivity/' + process_id + '/' + group_id);
    }

    excute_activity(obj, guid_next_activity, multi_arrow, variable_condition) {   
        return this.http.post(this.baseapi + this._controllerActivity + 'excuteActivity/' + guid_next_activity + '/' + multi_arrow + '/' + variable_condition, obj);
    }

    upload_files(formData) { 
        
        const uploadReq = new HttpRequest('POST', `${this.baseapi + this._controllerActivity}` + 'addVariableFile', formData, {
            reportProgress: true,
        });

        return this.http.request(uploadReq);
        // this.http.request(uploadReq).subscribe(event => {
        //     if (event.type === HttpEventType.UploadProgress) {
        //     // TODO 
        //     } else if (event.type === HttpEventType.Response) {
            
        //     }
        // });
    }

}
