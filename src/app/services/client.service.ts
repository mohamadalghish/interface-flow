import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, RequestOptions} from '@angular/http';
import { AuthService } from '../auth/auth.service';

import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class ClientService {
    private baseapi = environment.apiUrl;
    private baseurl = environment.baseUrl;

    constructor(private http: HttpClient,
                public auth: AuthService) { }

    getallclients() {
        return this.http.get(this.baseapi + 'Clients');
    }

    getallclientsnew() {
        return this.http.get(this.baseapi + 'Clients/New');
    }

    addclient(obj, staff_id, department_id) {
        
        return this.http.post(this.baseapi + 'Clients/' + staff_id + '/' + department_id, obj);
    }

    getclient(id) {
        return this.http.get(this.baseapi + 'Clients/' + id );
    }

    deleteclient(id) {
        return this.http.delete(this.baseapi + 'Clients/' + id );
    }

}
