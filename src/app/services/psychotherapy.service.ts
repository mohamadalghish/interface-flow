import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, RequestOptions} from '@angular/http';
import { AuthService } from '../auth/auth.service';

import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class PsychotherapyService {
    private baseapi = environment.apiUrl;
    private baseurl = environment.baseUrl;

    constructor(private http: HttpClient,
                public auth: AuthService) { }

    getallpsychotherapys(guid) {
        return this.http.post(this.baseapi + 'MedicalConcerns/GetByDepartment', guid);
    }

    addtreatment(obj, guid) {
        return this.http.put(this.baseapi + 'Treatments/' + guid, obj);
    }

}
