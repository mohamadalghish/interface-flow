import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http,Headers, RequestOptions} from '@angular/http';
import { AuthService } from '../auth/auth.service';

import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class VillaService {
    private baseapi = environment.apiUrl;    
    
    constructor(private http: HttpClient, public auth: AuthService) { }
 
    getallvillas() {        
        return this.http.get(this.baseapi + "villa/all");
    }

    addvilla(obj) {   
        return this.http.post(this.baseapi + "villa/create_villa", obj);
    }

}