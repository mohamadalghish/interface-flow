import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, RequestOptions} from '@angular/http';
import { AuthService } from '../auth/auth.service';

import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class DailysessionService {
    private baseapi = environment.apiUrl;
    private baseurl = environment.baseUrl;

    constructor(private http: HttpClient,
                public auth: AuthService) { }
    
    adddailysession(obj) {
        return this.http.post(this.baseapi + 'DailySessions', obj);
    }

    getalldailysession() {
        return this.http.get(this.baseapi + 'DailySessions');
    }

    deletedailysessions(id) {
        return this.http.delete(this.baseapi + 'DailySessions/' + id);
    }

}
