import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http,Headers, RequestOptions} from '@angular/http';
import { AuthService } from '../auth/auth.service';

import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class UserService {
    private baseapi = environment.apiUrl;   
    private baseurl = environment.baseUrl; 
    private _controller = "user/";
    
    constructor(private http: HttpClient, public auth: AuthService) { }
 
    get_all_users(page, size) {
        return this.http.get(this.baseapi + this._controller  + 'list/' + page + '/' + size );
    }

    save_user(obj) {
        return this.http.post(this.baseapi + this._controller  + 'save', obj);
    }

    get_user_by_id(id) {
        return this.http.get(this.baseapi + this._controller  + 'get/' + id );
    }

    delete_user_by_id(id) {
        return this.http.get(this.baseapi + this._controller  + 'delete/' + id );
    }

    get_by_email(email) {        
        return this.http.get(this.baseapi + this._controller + "get_by_email/" + email);
    }

    signin(obj) {
        return this.http.post(this.baseurl + 'auth', obj);
    }

}