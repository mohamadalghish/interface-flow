import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, RequestOptions} from '@angular/http';
import { AuthService } from '../auth/auth.service';

import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class MedicalconcernService {
    private baseapi = environment.apiUrl;
    private baseurl = environment.baseUrl;

    constructor(private http: HttpClient,
                public auth: AuthService) { }

    getallmedicalconcerns() {
        return this.http.get(this.baseapi + 'MedicalConcerns');
    }

    getmedicalconcerns(id) {
        return this.http.get(this.baseapi + 'MedicalConcerns/' + id);
    }

    getmedicalconcernstreatment(id) {
        return this.http.get(this.baseapi + 'MedicalConcerns/Treatment/' + id);
    }

    updatemedicalconcernsstatus(id, staff_id, status) {
        return this.http.get(this.baseapi + 'MedicalConcerns/Status/' + id + '/' + staff_id + '/' + status);
    }

    getmedicalconcernsforwadlist(id) {
        return this.http.get(this.baseapi + 'MedicalConcerns/MedicalConcernForwadList/' + id);
    }

    getallmedicalconcernsnew() {
        return this.http.get(this.baseapi + 'MedicalConcerns/New');
    }

    addMedicalconcern(obj) {
        return this.http.post(this.baseapi + 'MedicalConcerns', obj);
    }

    modifyMedicalconcern(obj, id) {

        return this.http.put(this.baseapi + 'MedicalConcerns/' + id, obj);
    }

}
