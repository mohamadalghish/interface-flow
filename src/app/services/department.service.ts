import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, RequestOptions} from '@angular/http';
import { AuthService } from '../auth/auth.service';

import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class DepartmentService {
    private baseapi = environment.apiUrl;
    private baseurl = environment.baseUrl;
    private _controllerGroup = "group/";

    constructor(private http: HttpClient,
                public auth: AuthService) { }

    

    get_all_departments(page, size) {
        return this.http.get(this.baseapi + this._controllerGroup  + 'list/' + page + '/' + size );
    }

    get_departments() {
        return this.http.get(this.baseapi + this._controllerGroup  + 'list_all' );
    }

    save_department(obj) {
        return this.http.post(this.baseapi + this._controllerGroup  + 'save', obj);
    }

    get_department_by_id(id) {
        return this.http.get(this.baseapi + this._controllerGroup  + 'get/' + id );
    }

    delete_department_by_id(id) {
        return this.http.get(this.baseapi + this._controllerGroup  + 'delete/' + id );
    }

}
