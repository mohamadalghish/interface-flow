import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { AuthService }      from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) {}

    // canActivate(
    //     next: ActivatedRouteSnapshot,
    //     state: RouterStateSnapshot): boolean {
    //     console.log('AuthGuard#canActivate called');
    //     return true;
    // }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): boolean {
        let url: string = state.url;

        // // using cookies
        // //check if past expiration date
        // var cookieExp = new Date();
        // //Set 'expires' option in 3 hours
        // cookieExp.setMinutes(cookieExp.getMinutes() + 180);
        // $cookies.putObject('LoggedUser', user, { expires: cookieExp });

        // using local storage
        var values = localStorage.getItem('token');
        if (values != null) {
            var value_str = values.split(";");
            var token_date = new Date(value_str[1]);
            if (token_date < new Date()) {
                localStorage.removeItem('token');
            }
        }

        return this.checkLogin(url);
      }

      checkLogin(url: string): boolean {
        if (this.authService.isLoggedIn) { return true; }
        if (localStorage.getItem('token') != null) { return true; }

        // Store the attempted URL for redirecting
        this.authService.redirectUrl = url;

        // Navigate to the login page with extras
        // here check the login
        this.router.navigate(['/session/signin']);
        return false;
      }

}
