import { Injectable } from '@angular/core';
import decode from 'jwt-decode';
import { tokenNotExpired } from 'angular2-jwt';

// import { Observable, of } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { of } from 'rxjs/observable/of';

import { tap, delay } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  isLoggedIn = false;

  constructor(private router: Router) {
  }

  // store the URL so we can redirect after logging in
  redirectUrl: string;

  login(): Observable<boolean> {
    return of(true).pipe(
      delay(1000),
      tap(val => this.isLoggedIn = true)
    );
  }

  logout(): void {
    this.isLoggedIn = false;
  }
  
  public getToken(): string {
    var values = localStorage.getItem('token');
    if (values != null) {
      var value_str = values.split(";");
      return value_str[0];
    }

    return "";
  }

  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    // return a boolean reflecting 
    // whether or not the token is expired
    return tokenNotExpired(null, token);
  }

}