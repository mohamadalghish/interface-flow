import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {
  MatIconModule,
  MatCardModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatTabsModule,
  MatRadioModule,
  MatListModule,
  MatProgressBarModule,
  MatSlideToggleModule,
  MatSelectModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { DepartmentsRoutes } from './departments.routing';
import { DepartmentListComponent } from './list/list.component';
import { DepartmentAddComponent } from './add/add.component';
import { DepartmentStaffListComponent } from './staff/list.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DepartmentsRoutes),
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatTabsModule,
    MatListModule,
    MatSlideToggleModule,
    MatSelectModule,
    FlexLayoutModule,
    FormsModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    FileUploadModule,
    MatRadioModule
  ],
  declarations: [
    DepartmentListComponent,
    DepartmentAddComponent,
    DepartmentStaffListComponent,
  ]
})

export class DepartmentsModule {}
