import { Component } from '@angular/core';
import { DepartmentService } from '../../services/department.service';
import { GeneralService } from '../../provider/general.service';

import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-table-department',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class DepartmentStaffListComponent {

  departments: any;
  staffs: any;
  department: any;
  department_id: any;

  list_Type: any[] = [{
    id: 0,
    name : 'Management'
  }, {
    id: 1,
    name : 'Medical'
  }];

  constructor(private departmentService: DepartmentService
    ,private generalService: GeneralService
    ,private route: ActivatedRoute) {

    this.route.queryParams.subscribe(params => {
      this.department_id = JSON.parse(params["id"]);
    })

  }

  ngOnInit() {

    // TODO need replace
    // this.departmentService.getalldepartmentsstaff(this.department_id).subscribe(res => {
    //   this.departments = res;
    //   this.staffs = this.departments[0].staff;
    // },
    // err => {
    //   console.log('Error- ' + err.message);
    // });


  }

  getNameType(id) {
    this.list_Type.forEach(element => {
      if (element.id == id) {
        return element.name;
      }
    });
  }

  edit(id) {

  }

  delete(id) {

    if (confirm('Are you sure you want to delete this row into the database?')) {
      
      // TODO need replace
      // this.departmentService.deletepartments(id).subscribe(res => {
      //     this.generalService.openSnackBar('Deleted');

      //     this.department = res;
      //     let index_tmp = -1;
      //     for (let index = 0; index < this.departments.length; index++) {
      //       const element = this.departments[index];
      //       if (element.id == this.department.id) {
      //         index_tmp = index;
      //       }
      //     }

      //     if (index_tmp != -1) {
      //       this.departments.splice(index_tmp, 1)  
      //     }

      // },
      // err => {
      //   this.generalService.openSnackBar('Error- ' + err.message);
      //   console.log("Error- " + err.message)
      // });

      
    } else {
        
    }
    
  }

}
