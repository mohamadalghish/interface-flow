import { Routes } from '@angular/router';

import { DepartmentListComponent } from './list/list.component';
import { DepartmentAddComponent } from './add/add.component';
import { DepartmentStaffListComponent } from './staff/list.component';

export const DepartmentsRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'list',
            component: DepartmentListComponent
        }, {
            path: 'add',
            component: DepartmentAddComponent
        }, {
            path: 'staff',
            component: DepartmentStaffListComponent
        }]
    }
];
