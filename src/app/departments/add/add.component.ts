import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { DepartmentService } from '../../services/department.service';
import { GeneralService } from '../../provider/general.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

const password = new FormControl('', Validators.required);
const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

@Component({
  selector: 'app-add-user',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class DepartmentAddComponent {

  // general
  public form: FormGroup;
  Theme = 'primary';
  result: any;
  uuidv4: any;
  obj : any;

  // custom
  list_permission: any;
  

  list_Type: any[] = [{
    id: 0,
    name : 'Management'
  }, {
    id: 1,
    name : 'Medical'
  }];

  list_color: any[] = [{
    value: '#ad2121',
    name : 'red'
  }, {
    id: '#1e90ff',
    name : 'blue'
  }, {
    id: '#e3bc08',
    name : 'yellow'
  }, {
    id: '#42ad18',
    name : 'green'
  }, {
    id: '#f44336',
    name : 'orange'
  }, {
    id: '#009688',
    name : 'sky'
  }];

  constructor(private departmentService: DepartmentService
    , private fb: FormBuilder
    , private generalService: GeneralService
    , private route: ActivatedRoute
    , private router: Router) {

      this.route.queryParams.subscribe(params => {

        if (params.hasOwnProperty('id')) {
          this.uuidv4 = JSON.parse(params['id']);
        }
      });

      if (this.uuidv4 == null || this.uuidv4 == undefined) {
        this.uuidv4 = this.generalService.uuidv4();
      } else {

        this.get_item_by_id();
      }

    }

  ngOnInit() {
    
    this.form = this.fb.group({
      id: [this.uuidv4],
      name: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])],
      code: [null, Validators.required],
      permission: [null, Validators.required],
      type: [null, Validators.required],
      color: [null, Validators.required],
    });

    this.list_permission = this.generalService.permission;
  }

  submit() {

    let permission_number = 0;

    if ( this.form.value.permission instanceof Array ) {
      this.form.value.permission.forEach(function(element) {
        permission_number += element.viewValue;
      });
    } else {
      permission_number = this.form.value.permission;
    }

    this.form.value.type = this.form.value.type.id;
    this.form.value.color = this.form.value.color.name;
    this.form.value.permission = permission_number;
    console.log(this.form.value);

    
    this.departmentService.save_department(this.form.value).subscribe(res => {
      
      this.result = res;
      if (this.result.responseCode == 200) {
        this.generalService.openSnackBar('Saved');
        this.router.navigate(['/departments/list']);
      } else {
        this.generalService.openSnackBar('Error- ' + this.result.message);
        console.log('Error- ' + this.result.message);
      }
      
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
      console.log('Error- ' + err.message);
    });

  }

  get_item_by_id() {

    this.departmentService.get_department_by_id(this.uuidv4).subscribe(res => {
          
      this.result = res;
      if (this.result.responseCode == 200) {

        this.obj = this.result.data;

        for(var key in this.obj) {
          
          let value = this.obj[key];

          if (key == 'type') {

            for (let i = 0; i < this.list_Type.length; i++) {
              const element = this.list_Type[i];
              if (element.id == value) {
                this.form.controls[key].setValue(element); 
              }
            }
          }

          else if (key == 'color') {
            
            for (let i = 0; i < this.list_color.length; i++) {
              const element = this.list_color[i];
              if (element.name == value) {
                this.form.controls[key].setValue(element); 
              }
            }
          }

          else if (key == 'permission') {
            
            let return_menu = [];
            for (let i = 0; i < this.list_permission.length; i++) {
              const element = this.list_permission[i];
              if ( (element.viewValue & value) == element.viewValue) {
                return_menu.push(element);
              }
            }
            this.form.controls[key].setValue(return_menu); 
          }
          else {
            this.form.controls[key].setValue(value); 
          }

        }

      } else {
        this.generalService.openSnackBar('Error- ' + this.result.message);
        console.log('Error- ' + this.result.message);
      }
      
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
      console.log('Error- ' + err.message);
    });
  }


}
