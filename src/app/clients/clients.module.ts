import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {
  MatIconModule,
  MatCardModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatTabsModule,
  MatRadioModule,
  MatListModule,
  MatProgressBarModule,
  MatSlideToggleModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatChipsModule,
  MatSelectModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { ClientsRoutes } from './clients.routing';
import { ClientListComponent } from './list/list.component';
import { ClientAddComponent } from './add/add.component';
import { ClientProcessListComponent } from './process/list.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ClientsRoutes),
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatTabsModule,
    MatListModule,
    MatSlideToggleModule,
    MatSelectModule,
    FlexLayoutModule,
    FormsModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    FileUploadModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatRadioModule
  ],
  declarations: [
    ClientListComponent,
    ClientAddComponent,
    ClientProcessListComponent,
  ]
})

export class ClientsModule {}
