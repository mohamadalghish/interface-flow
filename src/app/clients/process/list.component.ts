import { Component } from '@angular/core';
import { ClientService } from '../../services/client.service';
import { GeneralService } from '../../provider/general.service';
import { DepartmentService } from '../../services/department.service';

@Component({
  selector: 'app-table-client',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ClientProcessListComponent {


  clients: any;
  client: any;
  departments_medical: any;
  dic_departments_medical: any[] = [];
  color = '';

  constructor(private clientService: ClientService,
    private generalService: GeneralService,
    private departmentService: DepartmentService) {
  }

  ngOnInit() {

    this.clientService.getallclients().subscribe(res => {
      this.clients = res;
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
      console.log('Error- ' + err.message);
    });

    // TODO need replace
    // this.departmentService.getmedicaldepartments().subscribe(res => {
    //   this.departments_medical = res;
    //   this.departments_medical.forEach(element => {
    //     this.dic_departments_medical[element.guid] = element.name;
    //   });
       
    // },
    // err => {
    //   this.generalService.openSnackBar('Error- ' + err.message);
    //   console.log('Error- ' + err.message);
    // });

  }

  department_name(toDepStaffId) {
    
    if (toDepStaffId != undefined && toDepStaffId != null) {

      let arr = toDepStaffId.split(',');
      let tmpIdx = -1;
      let arr_name = [];

      for (let index = 0; index < arr.length; index++) {
        const element = arr[index];
        if (element == '') {
          tmpIdx = index;
        }

        arr_name.push(this.dic_departments_medical[element]);
      }

      if (tmpIdx != -1) {
        arr_name.splice(tmpIdx, 1);
      }
     
      return arr_name;
    } else {
      return [];
    }
    
  }

  //
  edit(id) {

  }

  delete(id) {

    if (confirm('Are you sure you want to delete this row into the database?')) {
      
      this.clientService.deleteclient(id).subscribe(res => {
          
          this.client = res;
          this.generalService.openSnackBar('Deleted');
          let index_tmp = -1;
          for (let index = 0; index < this.clients.length; index++) {
            const element = this.clients[index];
            if (element.id == this.client.id) {
              index_tmp = index;
            }
          }

          if (index_tmp != -1) {
            this.clients.splice(index_tmp, 1)  
          }
      },
      err => {
        this.generalService.openSnackBar('Error- ' + err.message);
        console.log("Error- " + err.message)
      });
    } else {
        
    }
    
  }

}
