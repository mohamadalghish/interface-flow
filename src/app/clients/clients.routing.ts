import { Routes } from '@angular/router';

import { ClientListComponent } from './list/list.component';
import { ClientAddComponent } from './add/add.component';
import { ClientProcessListComponent } from './process/list.component';

export const ClientsRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'list',
            component: ClientListComponent
        },{
            path: 'add',
            component: ClientAddComponent
        },{
            path: 'process',
            component: ClientProcessListComponent
        }]
    }
];
