export class Client {
    id: number;
    NameClient: string;
    DateBirht: string;
    Gender: string;
    Age: string;
    Address1: string;
    Address2: string;
    Phone: string;
    PhoneEmergency: string;
    NameMother: string;
    NameFather: string;
    OccupationMother: string;
    OccupationFather: string;
    EmailMother: string;
    EmailFather: string;
    Sibling: string;
    DateConsulation: string;
    TimeConsulation: string;
}
