import { Component } from '@angular/core';
import { ClientService } from '../../services/client.service';
import { GeneralService } from '../../provider/general.service';

@Component({
  selector: 'app-table-client',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ClientListComponent {


  clients: any;
  client: any;

  constructor(private clientService: ClientService,
    private generalService: GeneralService) {
  }

  ngOnInit() {
    this.clientService.getallclientsnew().subscribe(res => {
      this.clients = res;
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
      console.log('Error- ' + err.message);
    });
  }

  edit(id) {

  }

  delete(id) {

    if (confirm('Are you sure you want to delete this row into the database?')) {
      
      this.clientService.deleteclient(id).subscribe(res => {
          
          this.client = res;
          this.generalService.openSnackBar('Deleted');
          let index_tmp = -1;
          for (let index = 0; index < this.clients.length; index++) {
            const element = this.clients[index];
            if (element.id == this.client.id) {
              index_tmp = index;
            }
          }

          if (index_tmp != -1) {
            this.clients.splice(index_tmp, 1)  
          }
      },
      err => {
        this.generalService.openSnackBar('Error- ' + err.message);
        console.log("Error- " + err.message)
      });
    } else {
        
    }
    
  }

}
