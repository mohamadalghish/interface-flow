import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { ClientService } from '../../services/client.service';
import { UserService } from '../../services/user.service';
import { GeneralService } from '../../provider/general.service';
import { DepartmentService } from '../../services/department.service';
import { Router } from '@angular/router';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http'
import { environment } from '../../../environments/environment.prod';

@Component({
  selector: 'app-add-user',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class ClientAddComponent {

  private _baseapi = environment.apiUrl;
  public form: FormGroup;
  list_departmet: any;

  // Datepicker
  touch: boolean = true;
  yearView: boolean;
  startAt: Date;
  DateConsulation: Date;
  staffName: ''
  DateBirht: Date;
  lastDateInput: Date | null;
  lastDateChange: Date | null;
  files: any[] = [];
  Theme = 'primary';


  constructor(private clientService: ClientService,
    private fb: FormBuilder,
    private generalService: GeneralService,
    private router: Router,
    private userService: UserService,
    private departmentService: DepartmentService
    ,private http: HttpClient) {

      this.DateConsulation = new Date();
      this.startAt = new Date();
      this.staffName = this.generalService.user_info.name;
  }

  ngOnInit() {
    this.form = this.fb.group({
      NameClient: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])],
      DateBirht: [null],
      Gender: [null, Validators.required],
      NameMother: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])],
      NameFather: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])],
      OccupationMother: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])],
      OccupationFather: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])],
      EmailMother: [null, Validators.compose([Validators.required, CustomValidators.email])],
      EmailFather: [null, Validators.compose([Validators.required, CustomValidators.email])],
      Sibling: [null],

      Phone: [null, Validators.required],
      PhoneEmergency: [null],
      Mobile: [null],

      DateConsulation: [null],
      Department: [null],
      Address1: [null],
      Address2: [null],

    });


    // TODO need replace
    // this.departmentService.getalldepartmentsaba().subscribe(res => {
    //   this.list_departmet = res;
    // },
    // err => {
    //   this.generalService.openSnackBar('Error- ' + err.message);
    // });

  }

  submit() {

    const DepartmentId = this.form.value.Department.id;
    const StaffId = this.generalService.user_info.id;
    this.form.value.DateConsulation = this.DateConsulation;
    this.form.value.DateBirht = this.DateBirht;

    let idsFiles = [];
    this.files.forEach(element => {
      idsFiles.push(element.id);
    });

    const obj = {
      client: this.form.value,
      idsFiles: idsFiles
    }

    console.log(this.form.value);
    this.clientService.addclient(obj, StaffId, DepartmentId).subscribe(res => {
      this.generalService.openSnackBar('The process has been sent');
      this.router.navigate(['/clients/list'])
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
    });
  }

  onDateInput = (e: MatDatepickerInputEvent<Date>) => this.lastDateInput = e.value;
  onDateChange = (e: MatDatepickerInputEvent<Date>) => this.lastDateChange = e.value;

  delete_file(id) {

    if (id > -1) {
      for(var i = this.files.length - 1; i >= 0; i--) {
        if(this.files[i].id === id) {
          this.files.splice(i, 1);
        }
      }
    }
  }

  upload(files) {
    
    if (files.length === 0)
      return;

    const formData = new FormData();

    for (let file of files)
    {
      formData.append(file.name, file);
      this.files.push({
        id: -1,
        name: file.name,
        length: file.size,
        contentType: file.type,
        date: new Date()
      })
    }
      
    const uploadReq = new HttpRequest('POST', `${this._baseapi}Upload`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        // TODO 
      } else if (event.type === HttpEventType.Response) {
        const id = event.body.toString();
        this.files[this.files.length - 1].id = parseInt(id);
      }
    });
  }

}
