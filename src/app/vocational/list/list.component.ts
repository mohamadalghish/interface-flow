import { Component } from '@angular/core';
import { VocationalService } from '../../services/vocational.service';

@Component({
  selector: 'app-table-department',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class VocationalListComponent {

  columns = [
    { name: 'Name' },
    { name: 'Code' }
  ];

  departments: any;

  constructor(private vocationalService: VocationalService) {
  }

  ngOnInit() {
    this.vocationalService.getalldepartments().subscribe(res => {
        this.departments = res;
    },
    err => {
      console.log('Error- ' + err.message);
    });
  }
}
