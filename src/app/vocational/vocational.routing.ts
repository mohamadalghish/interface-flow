import { Routes } from '@angular/router';

import { VocationalListComponent } from './list/list.component';
import { VocationalAddComponent } from './add/add.component';

export const VocationalRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'list',
            component: VocationalListComponent
        }, {
            path: 'add',
            component: VocationalAddComponent
        }]
    }
];
