import { Routes } from '@angular/router';

import { AdminLayoutComponent, AuthLayoutComponent } from './core';

import { AuthGuard } from './auth/auth.guard';

export const AppRoutes: Routes = [{
  path: '',
  component: AdminLayoutComponent,
  canActivate: [AuthGuard],
  children: [{
    path: '',
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  }, {
    path: 'apps',
    loadChildren: './apps/apps.module#AppsModule'
  }, {
    path: 'users',
    loadChildren: './users/users.module#UsersModule'
  }, {
    path: 'clients',
    loadChildren: './clients/clients.module#ClientsModule'
  }, {
    path: 'departments',
    loadChildren: './departments/departments.module#DepartmentsModule'
  }, {
    path: 'medicalconcerns',
    loadChildren: './medicalconcerns/medicalconcerns.module#MedicalconcernsModule'
  }, {
    path: 'occupational',
    loadChildren: './occupational/occupational.module#OccupationalModule'
  }, {
    path: 'vocational',
    loadChildren: './vocational/vocational.module#VocationalModule'
  }, {
    path: 'dailysession',
    loadChildren: './dailysession/dailysession.module#DailysessionModule'
  }, {
    path: 'process',
    loadChildren: './process/activities.module#ActivitiesModule'
  }]
}, {
  path: '',
  component: AuthLayoutComponent,
  children: [{
    path: 'session',
    loadChildren: './session/session.module#SessionModule'
  }]
}, {
  path: '**',
  redirectTo: 'session/404'
}];
