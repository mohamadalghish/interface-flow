import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';


import { UserService } from '../../services/user.service';
import { GeneralService } from "../../provider/general.service";
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  public form: FormGroup;
  private _userIfno : any;
  result: any;
  

  constructor(private fb: FormBuilder
            , private router: Router
            , public userService: UserService
            , private generalService: GeneralService
            , private cookieService: CookieService) {}

  ngOnInit() {
    this.form = this.fb.group ( {
      email: [null, Validators.compose([Validators.required, CustomValidators.email])],
      password: [null , Validators.compose ( [ Validators.required ] )]
    } );
  }

  onSubmit() {
    this.userService.signin(this.form.value).subscribe(res => {

      this.result = res;
      this.store_data_localy('token' ,this.result.data.split(' ')[1], false);

      this.userService.get_by_email(this.form.value.email).subscribe(res => {

        this._userIfno = res;
        this.generalService.user_info = this._userIfno.data;
        this.store_data_localy('user_info', JSON.stringify(this._userIfno.data), true);

      },
      err => {
        this.generalService.openSnackBar("Error- " + err.message);
        console.log("Error- " + err)
        return;
      });  
     
      
    },
    err => {
      this.generalService.openSnackBar("Error- " + err.message);
      console.log("Error- " + err)
      return;
    });
  }


  store_data_localy(key_store, data_store, navigate) {
    
    var values = new Array();
    var oneday = new Date();
    oneday.setHours(oneday.getHours() + 24); //one day from now

    values.push(data_store);
    values.push(oneday);
    
    try {

        localStorage.setItem(key_store, values.join(";"));

        if (navigate) {
          this.generalService.openSnackBar("Welcome");
          this.router.navigate(['/']);
        }
    }
    catch (e) { }

  }

}
