import { Component } from '@angular/core';
import { UserService } from '../../services/user.service';
import { GeneralService } from '../../provider/general.service';

import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import {PagedData} from '../../paging/paged-data';
import {Page} from '../../paging/page';

@Component({
  selector: 'app-table-user',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class UserListComponent {

  // general
  rows: any;
  result: any;
  department: any;
  page = new Page();
  resultColumns: any[] = [];

  constructor(private userService: UserService,
    private generalService: GeneralService
    , private router: Router) {

      this.page.pageNumber = 0;
      this.page.size = 20;
  }

  ngOnInit() {
    
    this.setPage({ offset: 0 });

  }

  /**
   * Populate the table with new data based on the page number
   * @param page The page to select
   */
  setPage(pageInfo) {

    this.resultColumns = [];
    this.page.pageNumber = pageInfo.offset;
    this.userService.get_all_users(this.page.pageNumber, this.page.size).subscribe(res => {

      this.result = res;
      if (this.result.responseCode == 200) {

      	this.page.totalElements = this.result.data.totalElements;
        this.rows = this.result.data.content;

        if(this.rows.length > 0) {
          for(var key in this.rows[0]) {
            let value = this.rows[0][key];
            this.resultColumns.push({
              name: key,
              value: value,
              flexGrow: 1.0,
              minWidth: 100
            });
          }
        }

      } else {
        this.generalService.openSnackBar('Error- ' + this.result.message);
        console.log('Error- ' + this.result.message);
      }
      
    },
    err => {
      this.generalService.openSnackBar('Error- ' + err.message);
      console.log('Error- ' + err.message);
    });

  }

  edit(id) {

    const navigationExtras: NavigationExtras = {
      queryParams: {
          'id': JSON.stringify(id),
      }
    };
    this.router.navigate(['/users/add'], navigationExtras);
  }

  delete(id) {

    if (confirm('Are you sure you want to delete this row into the database?')) {

      this.userService.delete_user_by_id(id).subscribe(res => {
      
        this.result = res;
        if (this.result.responseCode == 200) {
          
          this.generalService.openSnackBar('Deleted');
          this.setPage({ offset: 0 });
  
        } else {
          this.generalService.openSnackBar('Error- ' + this.result.message);
          console.log('Error- ' + this.result.message);
        }
        
      },
      err => {
        this.generalService.openSnackBar('Error- ' + err.message);
        console.log('Error- ' + err.message);
      });
      
    } else {
        // do nothing
    }
    
  }


}
