import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { UserService } from '../../services/user.service';
import { DepartmentService } from '../../services/department.service';

import { ActivatedRoute, Router } from '@angular/router';

import { GeneralService } from "../../provider/general.service";

const password = new FormControl('', Validators.required);
const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

@Component({
  selector: 'app-add-user',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class UserAddComponent {
 
  // general
  public form: FormGroup;
  result: any;
  uuidv4: any;
  obj : any;

  // custom
  list_departmet: any;
  currentDepartment = '';

  constructor(private userService: UserService,
    private departmentService: DepartmentService,
    private fb: FormBuilder
    , private router: Router
    , private route: ActivatedRoute
    , private generalService: GeneralService) {


      this.route.queryParams.subscribe(params => {

        if (params.hasOwnProperty('id')) {
          this.uuidv4 = JSON.parse(params['id']);
        }
      });

      this.get_departments();

      if (this.uuidv4 == null || this.uuidv4 == undefined) {
        this.uuidv4 = this.generalService.uuidv4();
      } else {
        this.get_item_by_id()
      }

  }

  ngOnInit() {
    this.form = this.fb.group({
      id: [this.uuidv4],
      userName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])],
      groupUsers: [null],
      email: [null, Validators.compose([Validators.required, CustomValidators.email])],
      password: password,
      confirmPassword: confirmPassword
    });

    
    


  }

  get_departments() {
    this.departmentService.get_departments().subscribe(res => {
      
      this.result = res;
      if (this.result.responseCode == 200) {

      	this.list_departmet = this.result.data;
      } else {
        this.generalService.openSnackBar('Error- ' + this.result.message);
        console.log('Error- ' + this.result.message);
      }
      
    },
    err => {
      this.generalService.openSnackBar("Error- " + err.message)
    });
  }

  filterStates(val: string) {
    return val ? this._filter(this.list_departmet, val) : this.list_departmet;
  }

  private _filter(states: any[], val: string) {
    const filterValue = val.toLowerCase();
    return states.filter(state => state.name.toLowerCase().startsWith(filterValue));
  }

  displayFn(value: any): string {
    return value && typeof value === 'object' ? value.name : value;
  }
  
  submit() {
    
    var pwdChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    var pwdLen = 10;
    var randPassword = Array(pwdLen).fill(pwdChars).map(function(x) { return x[Math.floor(Math.random() * x.length)] }).join('');


    let groupUsersList = [];

    if ( this.form.value.groupUsers instanceof Array ) {

      for (let i = 0; i < this.form.value.groupUsers.length; i++) {
        const element = this.form.value.groupUsers[i];
        groupUsersList.push({
          id: element.id
        })
      }
    }

    this.form.value.groupUsers = groupUsersList;
    console.log(this.form.value)
    this.userService.save_user(this.form.value).subscribe(res => {
      this.generalService.openSnackBar('Added')
      this.router.navigate(['/users/list'])
    },
    err => {
      this.generalService.openSnackBar("Error- " + err.message)
    });
  }

  get_item_by_id() {
      this.userService.get_user_by_id(this.uuidv4).subscribe(res => {
          
          this.result = res;
          if (this.result.responseCode == 200) {
    
            this.obj = this.result.data;

            for(var key in this.obj) {
              
              let value = this.obj[key];

              if (key == 'groupUsers') {

                let listValue = [];
                let listValueTmp = [];

                for (let i = 0; i < value.length; i++) {
                  const element = this.list_departmet[i];
                  listValueTmp.push(element.id)
                }

                for (let i = 0; i < this.list_departmet.length; i++) {
                  const element = this.list_departmet[i];
                  if ( listValueTmp.includes(element.id) ) {
                    listValue.push(element);
                  }
                }
                this.form.controls[key].setValue(listValue);
              }
              else {
                if (this.form.controls.hasOwnProperty(key)) {
                  this.form.controls[key].setValue(value); 
                }
              }

            }
    
          } else {
            this.generalService.openSnackBar('Error- ' + this.result.message);
            console.log('Error- ' + this.result.message);
          }
          
        },
        err => {
          this.generalService.openSnackBar('Error- ' + err.message);
          console.log('Error- ' + err.message);
        });
  }

}
