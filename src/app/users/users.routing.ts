import { Routes } from '@angular/router';

import { UserListComponent } from './list/list.component';
import { UserAddComponent } from './add/add.component';

export const UsersRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'list',
            component: UserListComponent
        },{
            path: 'add',
            component: UserAddComponent
        }]
    }
];
