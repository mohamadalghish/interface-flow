import { Injectable } from '@angular/core';

import 'rxjs/add/observable/of';

import { Router } from '@angular/router';


import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material';

@Injectable()
export class GeneralService {

  user_info: any ;

  permission = [
    { value: 'Staff', viewValue: 2 },
    { value: 'Patient', viewValue: 4 },
    { value: 'Department', viewValue: 8 },
    { value: 'Medical Concern', viewValue: 16 },
    { value: 'Psychotherapy', viewValue: 32 },
    { value: 'Language & Speech', viewValue: 64 },
    { value: 'Occupational Therapy', viewValue: 128 },
    { value: 'Vocational Program', viewValue: 256 },
    { value: 'Special Education', viewValue: 512 },
    { value: 'Administration', viewValue: 1022 },
  ];

  colors: any = {
    red: {
      primary: '#ad2121',
      secondary: '#FAE3E3'
    },
    blue: {
      primary: '#1e90ff',
      secondary: '#D1E8FF'
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA'
    },
    green: {
      primary: '#cef7be',
      secondary: '#42ad18'
    },
    orange: {
      primary: '#ef948d52',
      secondary: '#f44336'
    },
    sky: {
      primary: '#0096881a',
      secondary: '#009688'
    }
  };

  constructor(private router: Router,
    public snackBar: MatSnackBar) {
  }

  openSnackBar(message) {
    const actionButtonLabel = 'Retry';
    const action = false;
    const setAutoHide = true;
    const autoHide = 4000;
    const addExtraClass = true;
    const horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    const verticalPosition: MatSnackBarVerticalPosition = 'top';

    const config = new MatSnackBarConfig();
    config.verticalPosition = verticalPosition;
    config.horizontalPosition = horizontalPosition;
    config.duration = setAutoHide ? autoHide : 0;
    config.panelClass = addExtraClass ? ['blue-snackbar'] : undefined;
    this.snackBar.open(message, action ? actionButtonLabel : undefined, config);
  }


  get_user_info_localy() {
    
    var values = localStorage.getItem('user_info');

    if (values != null) {
        var value_str = values.split(";");
        var token_date = new Date(value_str[1]);
        if (token_date < new Date()) {
            localStorage.removeItem('user_info');
        }
        this.user_info = JSON.parse(value_str[0]);
    }

  }


  uuidv4() {

    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }


}
