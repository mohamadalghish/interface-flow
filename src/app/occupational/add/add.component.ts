import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { OccupationalService } from '../../services/occupational.service';
import { GeneralService } from '../../provider/general.service';

const password = new FormControl('', Validators.required);
const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

@Component({
  selector: 'app-add-user',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class OccupationalAddComponent {

  public form: FormGroup;
  Theme = 'primary';
  list_permission: any;
  list_Type: any[] = [{
    id: 0,
    name : 'Management'
  }, {
    id: 1,
    name : 'Medical'
  }];

  constructor(private occupationalService: OccupationalService,
    private fb: FormBuilder,
    private generalService: GeneralService) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      Name: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])],
      Code: [null, Validators.required],
      Permission: [null, Validators.required],
      Type: [null, Validators.required],
    });

    this.list_permission = this.generalService.permission;
  }

  submit() {
    
    var permission_number = 0;
    this.form.value.Permission.forEach(function(element) {
      permission_number += element.viewValue;
    });

    this.form.value.Type = this.form.value.Type.id;
    this.form.value.Permission = permission_number;
    console.log(this.form.value)
    this.occupationalService.adddepartment(this.form.value).subscribe(res => {
      
    },
    err => {
      console.log('Error- ' + err.message);
    });
  }


}
