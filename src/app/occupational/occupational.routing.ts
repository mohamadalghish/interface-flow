import { Routes } from '@angular/router';

import { OccupationalListComponent } from './list/list.component';
import { OccupationalAddComponent } from './add/add.component';

export const OccupationalRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'list',
            component: OccupationalListComponent
        }, {
            path: 'add',
            component: OccupationalAddComponent
        }]
    }
];
