import { Component } from '@angular/core';
import { OccupationalService } from '../../services/occupational.service';

@Component({
  selector: 'app-table-department',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class OccupationalListComponent {

  columns = [
    { name: 'Name' },
    { name: 'Code' }
  ];

  departments: any;

  constructor(private occupationalService: OccupationalService) {
  }

  ngOnInit() {
    this.occupationalService.getalldepartments().subscribe(res => {
        this.departments = res;
    },
    err => {
      console.log('Error- ' + err.message);
    });
  }
}
