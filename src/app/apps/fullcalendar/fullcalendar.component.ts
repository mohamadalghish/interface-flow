import { Component, ChangeDetectionStrategy, Inject, ViewChild, TemplateRef } from '@angular/core';
import { MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-calendar-dialog',
  template: `
  <h5 class="mt-0">Event action occurred</h5>
  <div>
    Action:
    <pre>{{ data?.action }}</pre>
  </div>
  <div>
    Event:
    <pre>{{ data?.event | json }}</pre>
  </div>
  <button md-button type="button" (click)="dialogRef.close()">Close dialog</button>`
})
export class CalendarDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<CalendarDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }
}

import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';

import { Subject } from 'rxjs/Subject';

import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';

import { ScheduleService } from "../../services/schedule.service";
import { GeneralService } from "../../provider/general.service";

import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';



@Component({
  selector: 'app-fullcalendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './fullcalendar.component.html',
  styleUrls: ['./fullcalendar.component.scss']
})
export class FullcalendarComponent {

  dialogRef: MatDialogRef<CalendarDialogComponent>;
  lastCloseResult: string;
  actionsAlignment: string;
  config: MatDialogConfig = {
    disableClose: false,
    width: '',
    height: '',
    position: {
      top: '',
      bottom: '',
      left: '',
      right: ''
    },
    data: {
      action: '',
      event: []
    }
  };
  numTemplateOpens = 0;

  view = 'day';
  data_schedule : any
  viewDate: Date = new Date();

  modalData: {
    action: string,
    event: CalendarEvent
  };

  actions: CalendarEventAction[] = [{
    label: '<i class="editButton"></i>',
    onClick: ({event}: {event: CalendarEvent}): void => {
      this.handleEvent('Edited', event);
    }
  }, {
    label: '<i class="deleteButton"></i>',
    onClick: ({event}: {event: CalendarEvent}): void => {
      this.events = this.events.filter(iEvent => iEvent !== event);
      this.handleEvent('Deleted', event);
    }
  }];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];

  activeDayIsOpen = true;

  constructor(public dialog: MatDialog
    , private generalService: GeneralService
    , private scheduleService: ScheduleService
    , private router: Router) {}

  ngOnInit() {
    let staffId = this.generalService.user_info.id;
    
    this.scheduleService.getmycalendar(staffId).subscribe(res => {
      this.data_schedule = res;

      this.data_schedule.forEach(element => {

        let tmp = element.schedule;
        let obj = {
          client : tmp.clientId,
          date : tmp.date,
          timeFrom : tmp.timeFrom,
          timeTo : tmp.timeTo,
          duration : tmp.hour,
          staf_name : element.staff.name,
          department_name : element.staff.department.name,
          department_id : element.staff.departmentId,
          medical_concern_id : tmp.client.medicalConcern[0].id,
          color: element.staff.department.color
        }

        this.addEvent(obj);
      });
      this.refresh.next();
    },
    err => {
      console.log('Error- ' + err.message);
    });

  } 

  dayClicked({date, events}: {date: Date, events: CalendarEvent[]}): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  eventTimesChanged({event, newStart, newEnd}: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    // this.config.data = {event, action};
    // this.dialogRef = this.dialog.open(CalendarDialogComponent, this.config);

    // this.dialogRef.afterClosed().subscribe((result: string) => {
    //   this.lastCloseResult = result;
    //   this.dialogRef = null;
    // });
    const navigationExtras: NavigationExtras = {
      queryParams: {
          'id': JSON.stringify(event.id)
      }
    };

    this.router.navigate(['/dailysession/add'], navigationExtras);

  }

  addEvent(obj): void {
    let day = new Date(obj.date);
    let start_time_date = new Date(obj.timeFrom);
    let end_time_date = new Date(obj.timeTo);

    this.events.push({
      title: "Client " + obj.client + ", Staff " + obj.staf_name + ", Department " + obj.department_name + ", Duration " + obj.duration,
      start: new Date(day.getFullYear(), day.getMonth(), day.getDate(), start_time_date.getHours(), start_time_date.getMinutes(), start_time_date.getSeconds(), start_time_date.getMilliseconds()),
      end: new Date(day.getFullYear(), day.getMonth(), day.getDate(), end_time_date.getHours(), end_time_date.getMinutes(), end_time_date.getSeconds(), end_time_date.getMilliseconds()),
      color: this.generalService.colors[obj.color],
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      id: obj.medical_concern_id
    });
  }

}
