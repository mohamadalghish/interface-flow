import { Routes } from '@angular/router';

import { FullcalendarComponent } from './fullcalendar/fullcalendar.component';

export const AppsRoutes: Routes = [{
  path: '',
  children: [{
    path: 'calendar',
    component: FullcalendarComponent
  }]
}];
